    <script type="text/javascript">


         $('#qSubmit').fadeOut();

        var typeA = ["deviceMac", "cpeMac", "vlanId"];
        var typeB = ["&&", "AND"];
        var typeC = ["=="];
        var autocompleteList = ["deviceMac", "cpeMac", "vlanId", "&&", "AND", "=="];

        var prevQuery = '';
        var autoPosition = null;
        var prospectsList = [];

        function handleChangeQuery(query) {
            if(query.length !== prevQuery.length) {
                var queryValid = validateQuery(query);

                document.getElementById('query').classList.remove('valid');
                document.getElementById('query').classList.remove('invalid');
                if(queryValid) {
                    document.getElementById('query').classList.add('valid');

                    //show search

                    $('#qSubmit').fadeIn();


                } else {
                    document.getElementById('query').classList.add('invalid');

                    //hide search

                    $('#qSubmit').fadeOut();

                }

                prospectsList = [];
                var autocomplete = document.getElementById('autocomplete');
                var { diff, position } = getDifference(prevQuery, query);

                for(var i=0; i<autocompleteList.length; i++) {
                    if(diff === autocompleteList[i].slice(0, diff.length)) {
                        var querys = query.split(" ");
                        var already = false
                        if(typeA.includes(autocompleteList[i])) {
                            for(var j=0; j<querys.length; j++) {
                                if(autocompleteList[i] === querys[j]) {
                                    already = true;
                                }
                            }
                        }

                        if(!already) prospectsList.push(autocompleteList[i]);
                    }
                }

                var isValid = true;
                if(position !== 0 && query[position - 1] !== ' ') isValid = false;
                if(query.length !== position + diff.length && query[position + diff.length] !== ' ') isValid = false;

                if(isValid && prospectsList.length > 0) {
                    autoPosition = position;

                    autocomplete.innerHTML = "<ul id='autocomplete-body' class='autocomplete-body'></ul>";
                    var autocompleteBody = document.getElementById('autocomplete-body');

                    for(var j=0; j<prospectsList.length; j++) {
                        autocompleteBody.innerHTML += "<li onclick='handleClickItem("+j+")'>"+ prospectsList[j] +"</li>";
                    }
                } else {
                    prevQuery = query;
                    autocomplete.innerHTML = '';
                    autoPosition = null;
                    prospectsList = [];
                }
            }
        }

        function handleClickItem(index) {
            var item = prospectsList[index];
            if(typeA.includes(prospectsList[index])) {
                item = item+" ==";
            }
            item = item+" ";

            var output = [prevQuery.slice(0, autoPosition), item, prevQuery.slice(autoPosition + 1)].join('');
            document.getElementById('query').value = output;
            prevQuery = output;
            autocomplete.innerHTML = '';
            autoPosition = null;
            prospectsList = [];
        }

        function validateQuery(query) {
            var isValid = true;

            if(query.length > 0) {
                query = query.trim();
                query = query.replace(/\s\s+/g, ' ');
                var querys = query.split(" ");

                var i = 0;

                var allQuerys = [];
                for(var j=0; j<querys.length; j++) {
                    if(typeA.includes(querys[j])) {
                        if(allQuerys.includes(querys[j])) {
                            isValid = false;
                            break;
                        } else {
                            allQuerys.push(querys[j]);
                        }
                    }
                }

                if(isValid) {
                    while(i < querys.length) {
                        if(typeA.includes(querys[i]) && querys[i+1] == typeC[0] && querys[i+2]) {
                            if(querys[i] == typeA[0] || querys[i] == typeA[1]) {
                                if(querys[i+2].length !== 12) {
                                    isValid = false;
                                    break;
                                }
                            } else if(querys[i] == typeA[2]) {
                                if(Number(querys[i+2]) !== parseInt(Number(querys[i+2]), 10) || querys[i+2] < 1 || querys[i+2] > 4094) {
                                    isValid = false;
                                    break;
                                }
                            }

                            if(querys[i+3]) {
                                if(typeB.includes(querys[i+3]) && i + 4 < querys.length) {
                                } else {
                                    isValid = false;
                                    break;
                                }
                            }
                        } else {
                            isValid = false;
                            break;
                        }

                        i += 4;
                    }
                }
            }

            return isValid
        }

        function getDifference(a, b) {
            var i = 0;
            var j = 0;
            var diff = "";
            var position = null;

            while (j < b.length)
            {
                if (a[i] != b[j] || i == a.length) {
                    diff += b[j];
                    if(position === null) {
                        position = j;
                    }
                } else {
                    i++;
                }

                j++;
            }
            return { diff, position };
        }

        function submit() {
            var query = document.getElementById('query').value;
            var queryValid = validateQuery(query);

            if(queryValid && query.length > 0) {
                query = query.trim();
                query = query.replace(/\s\s+/g, ' ');
                var querys = query.split(" ");

                var i = 0;
                var result = {};

                while(i < querys.length) {
                    if(typeA.includes(querys[i])) {
                        result[querys[i]] = querys[i+2];
                    }
                    i += 4
                }

                console.log(result)
            }
        }
    </script>