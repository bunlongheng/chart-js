<style type="text/css">

    h1 {
        font-size: 15px;
    }

    .lastSelectedMenu {
        color: black;
    }

    .panel {
        text-align: center !important;
        /*margin: 0 auto;*/
        padding-left: 5%;
    }


    .panel .carousel ,.panel .chart {
        width: 90%;
    }


    .query {
        width: 70%;
        height: 40px;
        border: solid 2px #9B83BC;
        outline: none;
        color: #949ba2;
        border: none;
        border-radius: 4px;
        -webkit-box-shadow: none;
        box-shadow: none;
        -webkit-transition: none;
        -o-transition: none;
        transition: none;
        background-color: #494b54;
        font-size: 0.88rem;
        padding-right: 20px;
        text-indent: 10px;

        caret-color: #f7af3e;
    }
    .query.valid {
        border: solid 2px ;

        color: white;
        background-color: transparent;
        /* border-color: #1bbf89; */
        border-image: linear-gradient(90deg, rgba(48, 183, 149, 1.000) 30%, rgba(83, 189, 109, 1.000) 55%, rgba(130, 195, 65, 1.000) 100%);
        border-image-slice: 1;



    }
    .query.invalid {

        border: solid 2px ;

        color: white;
        background-color: transparent;
        /* border-color: #1bbf89; */
        border-image: linear-gradient(90deg, rgba(234, 134, 142, 1.000) 30%, rgba(228, 97, 108, 1.000) 55%, rgba(220, 54, 68, 1.000) 100%);
        border-image-slice: 1;


    }
    .autocomplete {
        position: absolute;
        top: 40px;
        left: 0;
        width: 20%;
        color: #f7af3e;
        font-style: bold;
    }
    .autocomplete-body {
        width: 100%;
        margin: 0;
        padding: 0;
        border: 1px solid #f7af3e;
        background: rgba(245, 167, 32, 0.2)

    }
    .autocomplete-body li {
        list-style-type: none;
        cursor: pointer;
        padding: 5px 15px;
    }
    .autocomplete-body li:hover {
        background: rgba(245, 167, 32, 0.2)
    }
    .autocomplete-body li:active {
        background: rgba(245, 167, 32, 0.2)
    }

    #qSubmit {
        margin-left: 20px;
    }


    @media screen and (min-width: 768px)
    .carousel-control .fa-chevron-left, .carousel-control .icon-prev {
        margin-left: -10px;
    }
    @media screen and (min-width: 768px)
    .carousel-control .fa-chevron-left, .carousel-control .fa-chevron-right, .carousel-control .icon-next, .carousel-control .icon-prev {
        width: 30px;
        height: 30px;
        margin-top: -10px;
        font-size: 30px;
    }
    .carousel-control .fa-chevron-left, .carousel-control .icon-prev {
        left: -25%;
        margin-left: -10px;
    }
    .carousel-control .fa-chevron-left, .carousel-control .fa-chevron-right, .carousel-control .icon-next, .carousel-control .icon-prev {
        position: absolute;
        top: 50%;
        z-index: 5;
        display: inline-block;
        margin-top: -10px;
    }


    .carousel {
        position: relative;
    }

    .carousel-inner {
        position: relative;
        width: 100%;
        overflow: hidden;
    }

    .carousel-control.left, .carousel-control.right {
        background: none;
    }

    .carousel-control {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 15%;
        font-size: 20px;
        color: #fff;
        text-align: center;
        text-shadow: 0 1px 2px rgba(0,0,0,.6);
        background-color: rgba(0,0,0,0);
        filter: alpha(opacity=50);
        opacity: .5;
    }


</style>