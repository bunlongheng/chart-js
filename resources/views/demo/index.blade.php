<html><head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" value="7Mj26XM0jqhSRobOsTDw6c88hgN1wf0pcorfuOnL">

	<!-- Page title -->
	<title>Charts</title>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

	<!-- Vendor styles -->

	<link rel="stylesheet" href="/luna/vendor/bootstrap/css/bootstrap.css">

	<!-- App styles -->
	<link rel="stylesheet" href="/luna/styles/pe-icons/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="/luna/vendor/animate.css/animate.css">
	<link rel="stylesheet" href="/luna/styles/stroke-icons/style.css">
	<link rel="stylesheet" href="/luna/styles/pe-icons/helper.css">
	<link rel="stylesheet" href="/luna/styles/toastr.min.css">
	<link rel="stylesheet" href="/luna/styles/style.css">


	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css">


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

	@include('demo.styles')

</head>
<body class="  pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
	<div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>

<div class="wrapper">

	<!-- Header-->
	<nav class="navbar navbar-expand-md navbar-default fixed-top">
		<div class="navbar-header">
			<div id="mobile-menu">
				<div class="left-nav-toggle">
					<a href="#">
						<i class="stroke-hamburgermenu"></i>
					</a>
				</div>
			</div>
			<a class="navbar-brand" href="#">

				{{-- <img src="/img/benu-white.png" width="120px" style="margin: -8px 0px -2px -17px;"> --}}
			</a>
		</div>
	</div>
</nav>

<section class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12">

				<div class="view-header">
					<div class="header-icon">
						<i class="fa fa-connectdevelop"></i>
					</div>
					<div class="header-title">

						<div class="row">

							<div class="col-lg-3">

								<h3 class="m-b-xs">Store Level Traffic</h3>

								<span > Current Interval:
									<span class="lastSelected"></span>
								</span>


							</div>
							<div class="col-lg-6">


								<div id="input-group" class="input-group">

									<input type="text" id="query" class="query" onkeyup="handleChangeQuery(this.value)" autocomplete="off" placeholder="cpeMac = 112233445566 && deviceMac == AABBCC112233 && vlanId == 100">

									<button class="btn btn-success" id="qSubmit" class="pull-right" onclick="submit();graphAll(interval);">submit</button>


									<div id="autocomplete" class="autocomplete"></div>


								</div>


							</div>
							<div class="col-lg-3">

								<small class="lastSelectedMenu">

									<select name="last" class="float-right">
										<option id="default" value="default">Select Interval</option>
										<option value="day">Day</option>
										<option value="week">Week</option>
										<option value="month">Month</option>
										<option value="year">Year</option>
									</select>
								</small>


							</div>

						</div>

					</div>
				</div>

				<hr>

			</div>
		</div>


		<div class="row">


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">

							<h1>User Traffic</h1>


							<div id="chart1Carousel" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<canvas width="200" height="100" id="chart1"></canvas>
									</div>
								</div>

								<!-- Left and right controls -->
								<a class="left carousel-control" href="#chart1Carousel" data-slide="prev">
									<span class="fa fa-chevron-left"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#chart1Carousel" data-slide="next">
									<span class="fa fa-chevron-right"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>


						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">

							<h1>Passersby Vs. Visitors</h1>

							<div id="chart2Carousel" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<canvas width="200" height="100" id="chart2"></canvas>
									</div>
								</div>

								<!-- Left and right controls -->
								<a class="left carousel-control" href="#chart2Carousel" data-slide="prev">
									<span class="fa fa-chevron-left"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#chart2Carousel" data-slide="next">
									<span class="fa fa-chevron-right"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>


						</div>
					</div>
				</div>
			</div>




		</div>


		<div class="row">


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">
							<div class="chart">
								<h1>Top Categories</h1>
								<canvas width="200" height="100" id="chart3"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">
							<div class="chart">
								<h1>Top Sites</h1>
								<canvas width="200" height="100" id="chart4"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-lg-12">

				<div class="view-header">
					<div class="header-icon">
						<i class="fa fa-wifi"></i>
					</div>
					<div class="header-title">
						<h3 class="m-b-xs">WiFi Usage</h3>
						<small>
							This section will list all our WiFi charts
						</small>
					</div>
				</div>

				<hr>

			</div>
		</div>

		<div class="row">


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">

							<h1>CPE Bandwidth Usage</h1>

							<div id="chart5Carousel" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<canvas width="200" height="100" id="chart5"></canvas>
									</div>
								</div>

								<!-- Left and right controls -->
								<a class="left carousel-control" href="#chart5Carousel" data-slide="prev">
									<span class="fa fa-chevron-left"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#chart5Carousel" data-slide="next">
									<span class="fa fa-chevron-right"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>

						</div>

					</div>
				</div>
			</div>


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">
							<div class="chart">
								<h1>Usage Duration</h1>
								<canvas width="200" height="100" id="chart6"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">
							<div class="chart">
								<h1>Device Bandwidth Usage</h1>
								<canvas width="200" height="100" id="chart7"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="panel panel-filled">
					<div class="panel-body">
						<div class="panel">
							<div class="chart">
								<h1>Device Type - by browser</h1>
								<canvas width="200" height="100" id="chart8"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>



	</div>
</section>


<!-- Vendor scripts -->
<script src="/luna/vendor/pacejs/pace.min.js"></script>
<script src="/luna/vendor/jquery/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="/luna/vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>

<script type="text/javascript">

	function formatBytes(bytes) {
		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		if (bytes == 0) return '0 Byte';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};

	function hexToRgb(hex, opacity=1) {

		var h=hex.replace('#', '');
		h =  h.match(new RegExp('(.{'+h.length/3+'})', 'g'));
		for(var i=0; i<h.length; i++)
			h[i] = parseInt(h[i].length==1? h[i]+h[i]:h[i], 16);
		if (typeof opacity != 'undefined')  h.push(opacity);
		return 'rgba('+h.join(',')+')';

	}

	var red           = '#F44336';
	var pink          = '#E91E63';
	var purple        = '#9C27B0';
	var deeppurple    = '#673AB7';
	var indigo        = '#3F51B5';
	var blue          = '#2196F3';
	var lightblue     = '#03A9F4';
	var cyan          = '#00BCD4';
	var teal          = '#009688';
	var green         = '#4CAF50';
	var lightgreen    = '#8BC34A';
	var lime          = '#CDDC39';
	var yellow        = '#FFEB3B';
	var amber         = '#FFC107';
	var orange        = '#FF9800';
	var deeporange    = '#FF5722';
	var brown         = '#795548';
	var gray          = '#9E9E9E';
	var bluegray      = '#607D8B';

	var getLiveChartData     = null;

	// var colors = [red, pink, purple, deeppurple, indigo, blue, lightblue, cyan, teal, green, lightgreen, lime, yellow, amber, orange, deeporange, brown, gray, bluegray];

	var colors = [pink, green, lightblue, deeppurple, yellow, teal, cyan, teal, green, lightgreen, lime, yellow, amber, orange, deeporange, brown, gray, bluegray];


	neonBgColors = [];
	neonBgBorders = [];
	for(var i=0; i<colors.length; i++){
		neonBgColors.push(hexToRgb(colors[i], opacity=.3));
		neonBgBorders.push(hexToRgb(colors[i], opacity=1));
	}


	var interval  = 'day';
	window.charts = [];

	function drawChart(configs,interval) {

		if(configs.duration == undefined){
			configs.duration = 0;
		}

		var data           = {};
		data.intervalType  = interval;
		data.duration      = configs.duration;
		data.objectAmmount = '5';

		$.ajax({
			method: 'POST',
			url: '/api/randomData',
			crossDomain: true,
			contentType: false,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('value'),
				"Accept": "application/json",
				"Content-Type": "application/x-www-form-urlencoded",
				"Cache-Control": "no-cache"
			},
			data: data,
			success: function(response){

				// 🚀
				// console.log(response);

				if(configs.datasets == false){
					configs.datasets = [{
						data: configs.data,
						borderWidth:1,
						hoverBorderWidth:2,
						hoverBorderColor:'#fff',
						backgroundColor: neonBgColors,
						borderColor: neonBgBorders

					}];
				}else {


					var datasets = [];
					for (i = 0; i < configs.objects.length; i++) {

						// console.log(i);

						datasets[i] = {};
						datasets[i].borderWidth      = 1;
						datasets[i].hoverBorderWidth = 2;
						datasets[i].hoverBorderColor = '#fff';
						datasets[i].backgroundColor  = hexToRgb(colors[i], opacity=.3);
						datasets[i].borderColor      = hexToRgb(colors[i], opacity=1);
						datasets[i].data             = response[i];
						datasets[i].label            = configs.objects[i];

					}

					configs.datasets = datasets;
					configs.labels   = response['labels'];

					// debugger;

				}

				var options = {
					type: configs.type, // bar, horizontalBar, pie, line, doughnut, radar, polarArea
					data: {
						labels: configs.labels,
						datasets: configs.datasets
					},
					options: {
						scales: {
							yAxes: [{
								display: true,
								ticks: {
									stepSize: 10
								}
							}]
						},

						title: {
							display: false,
							// text: configs.selectorId + ' = ' + configs.title,
							text: [ configs.title ],
							fontColor: "white",
							fontSize: 20,
						},

						subtitle: {
							text: 'This text has <b>bold</b>, <i>italic</i>, <span style="color: red">coloured</span>, <a href="http://example.com">linked</a> and <br/>line-broken text.'
						},


						legend: {
							display: configs.legend,
							position: 'bottom',
							labels: {
								fontColor: "white",
								fontSize: 12
							}
						},

					}
				}

				if(window[configs.selectorId] && window[configs.selectorId] !== null){
					if (typeof window[configs.selectorId].destroy === 'function') {
						window[configs.selectorId].destroy();
					}
					delete window[configs.selectorId];
				}

				var ctx = document.getElementById(configs.selectorId).getContext('2d');
				window[configs.selectorId] = new Chart(ctx, options);

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(JSON.stringify(jqXHR));
				console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
			}
		});

	}

	// bar, horizontalBar, pie, line, doughnut, radar, polarArea

	function graphAll(interval) {


		var configs        = {};
		configs.title      = 'User Traffic';
		configs.selectorId = 'chart1';
		configs.type       = 'line';
		configs.legend     = true;
		configs.objects    = ['Mexico','Brazil','USA'];
		configs.datasets   = true;

		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------


		var configs        = {};
		configs.title      = 'Passersby Vs. Visitors';
		configs.selectorId = 'chart2';
		configs.type       = 'bar';
		configs.legend     = true;
		configs.objects    = ['Passersby','Visitors'];
		configs.datasets   = true;


		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'Top Categories';
		configs.selectorId = 'chart3';
		configs.type       = 'horizontalBar';
		configs.legend     = false;
		configs.labels     = ['Social Networks','Government','Travel','News','Sport','Search','Business','Chat','Educational','Food'];
		configs.data       = [100,90,82,77, 66, 55, 43,33,22,11, 2];
		configs.datasets   = false;

		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'Top Sites';
		configs.selectorId = 'chart4';
		configs.type       = 'doughnut';
		configs.legend     = true;
		configs.labels     = ['Google.com','Facebook.com','Youtube.com','Netflix.com','others'];
		configs.data       = [5,4,3,2,1];
		configs.datasets   = false;

		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'CPE Bandwidth Usage';
		configs.selectorId = 'chart5';
		configs.type       = 'bar';
		configs.legend     = true;
		configs.labels     = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		configs.objects    = ['Burlington','Boston','Cambridge','Andover'];
		configs.datasets   = true;


		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'Usage Duration';
		configs.selectorId = 'chart6';
		configs.type       = 'polarArea';
		configs.legend     = true;
		configs.labels     = ['< 2 min', '2-5 min','5-15 min','15-30 min','30-60 min','> 1 hr'];
		configs.data       = [23,9,45,22,45,60];
		configs.datasets   = false;

		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'Device Bandwidth Usage';
		configs.selectorId = 'chart7';
		configs.type       = 'polarArea';
		configs.legend     = true;
		configs.labels     = ['iPhone 11 Pro','iPad Pro', 'MacBook Pro','Nest','Hue',];
		configs.data       = [53,74,85,25,13];
		configs.datasets   = false;

		drawChart(configs, interval);

		// ----------------------------------------------------------------------------------------------------

		var configs        = {};
		configs.title      = 'Device Type - by browser';
		configs.selectorId = 'chart8';
		configs.type       = 'pie';
		configs.legend     = true;
		configs.labels     = ['MacOS','Windows','iOS','Android','Linux','Others'];
		configs.data       = [3,4,5,6,7,8];
		configs.datasets   = false;

		drawChart(configs, interval);

		console.log(charts);



		// ----------------------------------------------------------------------------------------------------


		// bar, horizontalBar, pie, line, doughnut, radar, polarArea

	}

	$("select[name='last']").val('default');
	$(".lastSelected").text(interval);

	$("select[name='last']").on('change', function() {

		var selectedVal =$(this).val();

		interval = selectedVal;
		graphAll(interval);

		console.log($(this).closest('canvas'));
		var $canvas = $(this).closest('.lastSelectedMenu').next('canvas');
		console.log($canvas.prop('id'));


		$(".lastSelected").text(selectedVal);
		console.log(selectedVal);

		$("select[name='last']").val('default');
		$(".lastSelected")
		.animate({
			color: "#4BB7E8"
		}, 500)
		.animate({
			color: "#FFF"
		}, 200)
		.animate({
			color: "#4BB7E8"
		}, 500)
		.fadeIn(200);


	});

	graphAll(interval);


	// cleanup carousel
	if(interval == 'day'){
		$('.right.carousel-control').hide();
	}


	var duration = 0;


	$(document).on('click', '.carousel-control', function () {

		var direction = $(this).attr('class').replace('carousel-control','');
		//console.log($(this).prev('.carousel-inner').find('canvas').attr('id'));

		var chartId = $(this).closest('.carousel').find('canvas').attr('id')
		console.log('%c chartId = ' + chartId, "color: green;");

		//Reset chart data
		$('#'  + chartId + 'Carousel canvas').remove();
		$('#'  + chartId + 'Carousel .carousel-inner').prepend('<div class="item active"> <canvas width="200" height="100" id="'+chartId+'"></canvas> </div>');

		function renderCarouselCharts(chartId) {

			switch(chartId) {

				case 'chart1':
				var configs        = {};
				configs.duration   = duration;
				configs.title      = 'User Traffic';
				configs.selectorId = 'chart1';
				configs.type       = 'line';
				configs.legend     = true;
				configs.objects    = ['Mexico','Brazil','USA'];
				configs.datasets   = true;
				drawChart(configs, interval);
				break;

				case 'chart2':
				var configs        = {};
				configs.title      = 'Passersby Vs. Visitors';
				configs.selectorId = 'chart2';
				configs.type       = 'bar';
				configs.legend     = true;
				configs.objects    = ['Passersby','Visitors'];
				configs.datasets   = true;
				drawChart(configs, interval);
				break;

				case 'chart5':

				var configs        = {};
				configs.title      = 'CPE Bandwidth Usage';
				configs.selectorId = 'chart5';
				configs.type       = 'bar';
				configs.legend     = true;
				configs.labels     = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
				configs.objects    = ['Burlington','Boston','Cambridge','Andover'];
				configs.datasets   = true;


				drawChart(configs, interval);

				default:
				// code block
			}

		}

		if(interval == 'day' || interval == 'week' || interval == 'month' || interval == 'year'){

			$('#'  + chartId + 'Carousel .right.carousel-control').show();

			if (direction.indexOf('left') !== -1) {
				duration--;
			} else {
				duration++;
			}

			renderCarouselCharts(chartId)

		}

	});


</script>

@include('demo.scripts.query')


</body>
</html>