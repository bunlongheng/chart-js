<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;


class ApiController extends Controller
{

	public function welcome()
	{
		return 'Welcome to operation API';
	}

	public function demo()
    {
        return view('demo.index',get_defined_vars());
    }

	public static function randomData(Request $request) {


		$today         = date("y-m-d");
		$now           = Carbon::now('America/New_York');
		$dt            = Carbon::parse($now);


		$h             = $dt->hour;
		$d             = $dt->day;
		$w             = $dt->week;
		$m             = $dt->month;
		$y             = $dt->year;


        // dd($h,$d,$w,$m,$y);

		$inputs        = $request->all();
		$intervalType  = $inputs['intervalType'];
		$objectAmmount = $inputs['objectAmmount'];
		$duration      = $inputs['duration'];


		switch ($intervalType) {

			case "day":
			$interval = $h+1; //24 hours in a day
			$minData = 10;
			$maxData = 100;
			$labels = ['12 AM','1 AM', '2 AM', '3 AM', '4 AM', '5 AM', '6 AM','7 AM','8 AM','9 AM','10 AM','11 AM','12 PM','1 PM','2 PM','3 PM','4 PM','5 PM','6 PM','7 PM', '8 PM', '9 PM', '10 PM', '11 PM'] ;
			break;

			case "week":
			$interval = $w+1; //Monday = 1
			$minData = 100;
			$maxData = 1000;
			$labels = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			break;

			case "month":
			$interval = 4;
			$minData = 2000;
			$maxData = 4000;
			$labels = ['Week 1','Week 2','Week 3','Week 4','Week 5'];
			break;

			case "year":
			$interval = $m; //Jan = 1
			$minData = 9000;
			$maxData = 12000;
			$labels = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			break;

			default:
			echo "Your interval is not in scope.";
		}

		$result = [];
		for ($i = 0; $i < $objectAmmount; $i++) {
			for ($j = 0; $j < $interval; $j++) {

				$result[$i][$j] = rand($minData,$maxData);
			}
		}

		$result['labels'] = $labels;

		return $result;

	}

}